﻿using System;
using System.Windows.Forms;

namespace NotifyMyLife {
    class NotifyMyLifeContext : ApplicationContext {
        private NotifyIcon _trayIcon;
        private ContextMenuStrip _trayIconContextMenu;
        private ToolStripMenuItem _closeMenuItem;

        public NotifyMyLifeContext() {
            Application.ApplicationExit += OnApplicationExit;
            InitializeComponent();
            _trayIcon.Visible = true;
        }
        private void InitializeComponent() {
            _trayIcon = new NotifyIcon
            {
                BalloonTipIcon = ToolTipIcon.Info,
                BalloonTipText = "I noticed you double clicked me! what can i do for you?",
                BalloonTipTitle = "You called?",
                Text = "fab app text bro",
                Icon = Properties.Resources.NotifyMyLife
            };

            //optional doubleclick
            _trayIcon.DoubleClick += TrayIcon_DoubleClick;

            // optional icon menu   
            _trayIconContextMenu = new ContextMenuStrip();
            _closeMenuItem = new ToolStripMenuItem();
            _trayIconContextMenu.SuspendLayout();

            //TrayIconContextMenu

            _trayIconContextMenu.Items.AddRange(new ToolStripItem[] {
                _closeMenuItem});
            _trayIconContextMenu.Name = "_trayIconContextMenu";
            _trayIconContextMenu.Size = new System.Drawing.Size(153, 70);

            //closeMenuItem
            _closeMenuItem.Name = "_closeMenuItem";
            _closeMenuItem.Size = new System.Drawing.Size(152, 22);
            _closeMenuItem.Text = "close the tray icon program";
            _closeMenuItem.Click += CloseMenuItem_Click;

            _trayIconContextMenu.ResumeLayout(false);
            _trayIcon.ContextMenuStrip = _trayIconContextMenu;

        }

        private void CloseMenuItem_Click(object sender, EventArgs e) {
            if(MessageBox.Show("Do You Really Want To Close Me?","Are You Sure?",MessageBoxButtons.YesNo,MessageBoxIcon.Exclamation,MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                Application.Exit();
            }
        }

        private void TrayIcon_DoubleClick(object sender, EventArgs e) {
            _trayIcon.ShowBalloonTip(10000);
        }

        private void OnApplicationExit(object sender, EventArgs e) {
            _trayIcon.Visible = false;
        }

    }
}
